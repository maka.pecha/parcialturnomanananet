﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace parcialMañanaProg3.Models
{
    public class Socio
    {
        public int id { get; set; }
        [Required(ErrorMessage = "El nombre es requerido")]
        public string nombre { get; set; }
        [Required(ErrorMessage = "El apellido es requerido")]
        public string apellido { get; set; }
        [Required(ErrorMessage = "El tipo de documento es requerido")]
        public int idTipoDocumento { get; set; }
        [Required(ErrorMessage = "El número de documento es requerido")]
        public string nroDocumento { get; set; }
        [Required(ErrorMessage = "El deporte es requerido")]
        public int idDeporte { get; set; }

    }
}