﻿using parcialMañanaProg3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace parcialMañanaProg3.ViewModels
{
    public class SocioVM
    {
        public Socio socioModel { get; set; }
        public List<TipoDocumento> tipoDocumentoSocio  { get; set; }
        public List<Deporte> deporte  { get; set; }
    }
}