﻿using parcialMañanaProg3.DataTransferObjects;
using parcialMañanaProg3.Models;
using parcialMañanaProg3.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace parcialMañanaProg3.AccesoDatos
{
    public class AD_Socios
    {
        public static bool InsertarNuevoSocio(Socio soc)
        {
            bool resultado = false;

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "insert into Socios values(@Nombre, @Apellido, @IdTipoDocumento, @NroDocumento, @IdDeporte)";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Nombre", soc.nombre); //asigno parametros al command
                cmd.Parameters.AddWithValue("@Apellido", soc.apellido);
                cmd.Parameters.AddWithValue("@IdTipoDocumento", soc.idTipoDocumento);
                cmd.Parameters.AddWithValue("@NroDocumento", soc.nroDocumento);
                cmd.Parameters.AddWithValue("@IdDeporte", soc.idDeporte);

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta
                cmd.ExecuteNonQuery(); //ejecuta la sentencia
                resultado = true; // si no hubo error es true
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static List<SocioDTO> ObtenerListaSocios()
        {
            List<SocioDTO> resultado = new List<SocioDTO>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = @"select s.Id, s.Nombre, Apellido, td.Nombre as TipoDocumento, s.NroDocumento, d.Nombre as Deporte  
                                    from Socios s
                                    join TiposDocumentos td on s.IdTipoDocumento=td.Id
									join Deportes d on d.Id = s.IdDeporte";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        SocioDTO aux = new SocioDTO();
                        aux.id = int.Parse(dr["Id"].ToString());
                        aux.nombre = dr["Nombre"].ToString();
                        aux.apellido = dr["Apellido"].ToString();
                        aux.tipoDocumento = dr["TipoDocumento"].ToString();
                        aux.nroDocumento = dr["NroDocumento"].ToString();
                        aux.deporte = dr["Deporte"].ToString();

                        resultado.Add(aux);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }
        public static Socio ObtenerSocio(int idSocio)
        {
            Socio resultado = new Socio();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "select * from Socios where id = @Id";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@Id", idSocio);

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        resultado.id = int.Parse(dr["Id"].ToString());
                        resultado.nombre = dr["Nombre"].ToString();
                        resultado.apellido = dr["Apellido"].ToString();
                        resultado.idTipoDocumento = int.Parse(dr["IdTipoDocumento"].ToString());
                        resultado.nroDocumento = dr["NroDocumento"].ToString();
                        resultado.idDeporte = int.Parse(dr["IdDeporte"].ToString());
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static List<TipoDocumento> ObtenerListaTipoDocumento()
        {
            List<TipoDocumento> resultado = new List<TipoDocumento>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "select * from TiposDocumentos";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        TipoDocumento aux = new TipoDocumento();
                        aux.id = int.Parse(dr["id"].ToString());
                        aux.nombre = dr["nombre"].ToString();

                        resultado.Add(aux);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static List<Deporte> ObtenerListaDeporte()
        {
            List<Deporte> resultado = new List<Deporte>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = "select * from Deportes";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        Deporte aux = new Deporte();
                        aux.id = int.Parse(dr["id"].ToString());
                        aux.nombre = dr["nombre"].ToString();

                        resultado.Add(aux);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }

        public static List<CantidadPorDeporteDTO> ObtenerListaCantidadPorDeporte()
        {
            List<CantidadPorDeporteDTO> resultado = new List<CantidadPorDeporteDTO>();

            string cadenaConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaBD"].ToString();

            SqlConnection cn = new SqlConnection(cadenaConexion);

            try
            {
                SqlCommand cmd = new SqlCommand();

                string consulta = @"select d.Nombre, count(*) as Cantidad
                                    from Socios s
                                    join Deportes d on s.IdDeporte=d.Id
                                    group by d.Nombre;";
                cmd.Parameters.Clear();

                cmd.CommandType = System.Data.CommandType.Text; //porque yo le mando la sentencia
                cmd.CommandText = consulta; //esta es la sentencia

                //la consulta
                cn.Open(); //abro la conexion
                cmd.Connection = cn; //le indico la conexion a la que se va a conectar y ya está abierta

                SqlDataReader dr = cmd.ExecuteReader(); // se instancia con el resultado de la operacion

                if (dr != null)
                {
                    while (dr.Read())
                    {
                        CantidadPorDeporteDTO aux = new CantidadPorDeporteDTO();
                        aux.nombre = dr["Nombre"].ToString();
                        aux.cantidad = int.Parse(dr["Cantidad"].ToString());

                        resultado.Add(aux);
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();//si hay o no error igual se cierra la conexion
            }

            return resultado;
        }


    }
}