﻿using parcialMañanaProg3.ViewModels;
using parcialMañanaProg3.AccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using parcialMañanaProg3.DataTransferObjects;

namespace parcialMañanaProg3.Controllers
{
    public class SocioController : Controller
    {
        // GET: Persona
        public ActionResult AltaSocio()
        {
            SocioVM vm = new SocioVM();
            vm.tipoDocumentoSocio = AD_Socios.ObtenerListaTipoDocumento();
            vm.deporte = AD_Socios.ObtenerListaDeporte();

            return View(vm);
        }
        // para hacer el post de la nueva persona y ver la lista
        [HttpPost]
        public ActionResult AltaSocio(SocioVM model)
        {
            if (ModelState.IsValid)
            {
                bool resultado = AD_Socios.InsertarNuevoSocio(model.socioModel);
                if (resultado)
                {
                    return RedirectToAction("ListadoSocios", "Socio");
                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        // get de la lista
        public ActionResult ListadoSocios()
        {
            List<SocioDTO> lista = AD_Socios.ObtenerListaSocios();
            return View(lista);
        }

        public ActionResult Reportes()
        {
            List<CantidadPorDeporteDTO> lista = AD_Socios.ObtenerListaCantidadPorDeporte();

            ReportesVM vm = new ReportesVM();
            vm.cantidadPorDeporte = lista;

            return View(vm);
        }
    }
}